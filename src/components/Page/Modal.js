// import React from "react";
// import { motion } from "framer-motion";
// const Modal = ({ selected, setSelected }) => {
//   if (!selected) {
//     return <></>;
//   }
//   return (
//     <>
//       <div
//         onClick={() => setSelected(null)}
//         className="fixed inset-0 bg-black/50 z-50 cursor-pointer overflow-y-scroll"
//       >
//         <div
//           onClick={(e) => e.stopPropagation()}
//           className="w-full max-auto my-8 px-8 cursor-default "
//         ></div>
//       </div>
//     </>
//   );
// };
// export default Modal;

import React from "react";
import { motion } from "framer-motion";

export default function Modal({ selected, setSelected }) {
  if (!selected) {
    return <></>;
  }

  return (
    <div
      onClick={() => setSelected(null)}
      className="fixed inset-0 bg-black/50 z-50 cursor-pointer overflow-y-scroll"
    >
      <div
        onClick={(e) => e.stopPropagation(console.log("handleParentClick"))}
        className="w-full max-w-[700px] mx-auto my-8 px-8 cursor-default"
      >
        <div>
          <img src={selected.url} className="w-full" />
        </div>
      </div>
    </div>
  );
}
