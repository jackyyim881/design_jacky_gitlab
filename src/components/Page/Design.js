import React from "react";
import { motion } from "framer-motion";
const Design = () => {
  return (
    <>
      <motion.div
        className="home"
        style={{ backgroundColor: "#f5f5f5" }}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
        <div className="design">
          <div className="design-inner">
            <div className="design-content">
              <h1>Design</h1>
              <p>
                Design is the process of creating a solution to a problem. It’s
                the process of creating something new, something that didn’t
                exist before. It’s the process of creating something that is
                useful, usable, and desirable.
              </p>
              <p>
                Design is the process of creating a solution to a problem. It’s
                the process of creating something new, something that didn’t
                exist before. It’s the process of creating something that is
                useful, usable, and desirable.
              </p>
            </div>
          </div>
        </div>
      </motion.div>
    </>
  );
};

export default Design;
