import React from "react";
import { motion, AnimatePresence } from "framer-motion";
import { useState } from "react";
import { items } from "./data";
import Modal from "./Modal";
import List from "./List";

const About = () => {
  const [selected, setSelected] = useState(null);
  return (
    <>
      <List setSelected={setSelected} />
      <Modal selected={selected} setSelected={setSelected} />
    </>
  );
};
export default About;

// const List = ({ setSelected }) => {
//   return (
//     <>
//       <div className="p-4">
//         <h1 className="text-center text-4xl mb-8 font-bold">About</h1>
//         <div className="columns-2 md:columns-3 xl:columns-4 gap-8">
//           {items.map((item) => (
//             <Card key={item.id} setSelected={setSelected} item={item} />
//           ))}
//         </div>
//       </div>
//     </>
//   );
// };

{
  /* <div className="p-4">
  <h1 className="text-center text-4xl mb-8 font-bold">About</h1>
  <div>
  {items.map((item) => (
    <Card key={item.id} setSelected={setSelected} item={item} />
  ))}
  </div>
</div> */
}
{
  /* <AnimatePresence>
{selectedId && (
  <motion.div layoutId={selectedId}>
  <motion.h5>{item.subtitle}</motion.h5>
  <motion.h2>{item.title}</motion.h2>
      <motion.button onClick={() => setSelectedId(null)} />
      </motion.div>
  )}
</AnimatePresence> */
}

{
  /* <motion.div variants={itemMain} className="transition-image-about">
  <motion.img
    src={process.env.PUBLIC_URL + `../images/image-2.jpg`}
    layoutId="main-image-1"
    transition={{
      ease: [0.6, 0.01, -0.05, 0.9],
      duration: 1.6,
    }}
    />
  </motion.div> */
}
// const itemMain = {
//   hidden: { opacity: 0, y: 4200 },
//   show: {
//     opacity: 1,
//     y: 0,
//     transition: {
//       ease: [0.6, 0.01, 0.05, 0.95],
//       duration: 1.6,
//     },
//   },
// };
