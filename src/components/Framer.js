// import React from "react";
// import { useRoutes } from "react-router-dom";
// import { motion, AnimatePresence } from "framer-motion";
// // const icon = {
// //   hidden: {
// //     pathLength: 0,
// //     fill: "rgba(255, 255, 255, 0)",
// //   },
// //   visible: {
// //     pathLength: 1,
// //     fill: "rgba(255, 255, 255, 1)",
// //   },
// // };

// function Framer({ Component, pageProps }) {
//   const router = useRoutes();

//   return (
//     <>
//       <AnimatePresence>
//         <motion.div
//           key={router.route}
//           className="base-page-size"
//           initial="initialState"
//           animate="animateState"
//           exit="exitState"
//           transition={{
//             duration: 0.75,
//           }}
//           variants={{
//             initialState: {
//               opacity: 0,
//             },
//             animateState: {
//               opacity: 1,
//             },
//             exitState: {
//               opacity: 0,
//               transition: {
//                 duration: 1,
//               },
//             },
//           }}
//         >
//           <Component {...pageProps} />
//         </motion.div>
//       </AnimatePresence>
//     </>
//   );
// }

// export default Framer;
