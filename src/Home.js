import { useEffect, useState } from "react";
import Loader from "./components/Loader";
import Banner from "./components/Banner";
import { LayoutGroup, AnimatePresence, motion } from "framer-motion";
import styled, { css } from "styled-components";

const TransitionImage = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    width: 800px;
    display: flex;
  }

  &.final {
    display: block;
    top: -128px;
    position: relative;
    width: 90%;
    margin: 0 auto;
    z-index: -100;

    @media (max-width: 480px) {
      top: -56px;
    }

    img {
      width: 100%;
      max-width: 100%;
    }
  }
`;

function Home() {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    loading
      ? document.querySelector("body").classList.add("loading")
      : document.querySelector("body").classList.remove("loading");
  }, [loading]);

  return (
    <>
      <LayoutGroup>
        <AnimatePresence>
          {loading ? (
            <motion.div key="loader">
              <Loader setLoading={setLoading} />
            </motion.div>
          ) : (
            <>
              <Banner />
              {!loading && (
                <motion.div className="transition-image final">
                  <motion.div>
                    <motion.img
                      src={process.env.PUBLIC_URL + `/images/image-2.jpg`}
                      layoutId="main-image-1"
                      transition={{
                        ease: [0.6, 0.01, 0.05, 0.9],
                        duration: 1.6,
                      }}
                    />
                  </motion.div>
                </motion.div>
              )}
            </>
          )}
        </AnimatePresence>
      </LayoutGroup>
    </>
  );
}

export default Home;
