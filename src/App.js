import React from "react";
import { useState, useEffect } from "react";

import Home from "./Home";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  useLocation,
} from "react-router-dom";
import "./index.css";
import "./sass/main.scss";

import List from "./components/Page/About";
import Header from "./components/Header";
import { motion, AnimatePresence } from "framer-motion";
import Case from "./components/Page/Case";
import Design from "./components/Page/Design";
import Strategy from "./components/Page/Strategy";
import About from "./components/Page/About";

function App() {
  const [selected, setSelected] = useState(null);
  return (
    <>
      <div className="App">
        <Header />
        <AnimatePresence>
          <Routes>
            <Route>
              <Route path="/" element={<Home />} />
              <Route path="/about" element={<About />} />
              <Route path="/cases" element={<Case />} />
              <Route path="/design" element={<Design />} />
              <Route path="/strategy" element={<Strategy />} />
            </Route>
          </Routes>
        </AnimatePresence>
      </div>
    </>
  );
}

export default App;
